// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"data.json":[function(require,module,exports) {
module.exports = [{
  "ID": 1,
  "titre": "Escapade à Valparaiso",
  "description": "Superatis Tauri montis verticibus qui ad solis",
  "imageUrl": "https://picsum.photos/400?image=1072",
  "theme": "Exotisme",
  "dateDebut": "10/25/2019",
  "dateFin": "10/29/2019",
  "prix": 986,
  "promotion": false
}, {
  "ID": 2,
  "titre": "Bankgok sous les étoiles",
  "description": "Isdem diebus Apollinaris Domitiani gener, paulo",
  "imageUrl": "https://picsum.photos/400?image=1081",
  "theme": "Exotisme",
  "dateDebut": "11/24/2019",
  "dateFin": "11/30/2019",
  "prix": 453,
  "promotion": true
}, {
  "ID": 3,
  "titre": "Si tu vas à Rio",
  "description": "His cognitis Gallus ut serpens adpetitus telo vel",
  "imageUrl": "https://picsum.photos/400?image=1074",
  "theme": "Farniente",
  "dateDebut": "08/12/2019",
  "dateFin": "08/22/2019",
  "prix": 390,
  "promotion": false
}, {
  "ID": 4,
  "titre": "Tokyo Hotel",
  "description": "Intellectum est enim mihi quidem in multis, et",
  "imageUrl": "https://picsum.photos/400?image=1070",
  "theme": "Culture",
  "dateDebut": "09/07/2019",
  "dateFin": "09/21/2019",
  "prix": 4333,
  "promotion": false
}, {
  "ID": 5,
  "titre": "Cahors by night",
  "description": "Metuentes igitur idem latrones Lycaoniam magna",
  "imageUrl": "https://picsum.photos/400?image=1067",
  "theme": "Authentique",
  "dateDebut": "06/07/2019",
  "dateFin": "06/13/2019",
  "prix": 832,
  "promotion": false
}, {
  "ID": 6,
  "titre": "Malaise en Malaisie",
  "description": "Saepissime igitur mihi de amicitia cogitanti",
  "imageUrl": "https://picsum.photos/400?image=1065",
  "theme": "Aventure",
  "dateDebut": "08/11/2019",
  "dateFin": "08/11/2019",
  "prix": 2342,
  "promotion": false
}, {
  "ID": 7,
  "titre": "Fièvre à Bali",
  "description": "Alios autem dicere aiunt multo etiam inhumanius",
  "imageUrl": "https://picsum.photos/400?image=1061",
  "theme": "Aventure",
  "dateDebut": "10/07/2019",
  "dateFin": "10/28/2019",
  "prix": 847,
  "promotion": false
}, {
  "ID": 8,
  "titre": "New-York la nuit",
  "description": "Quod si rectum statuerimus vel concedere amicis,",
  "imageUrl": "https://picsum.photos/400?image=1048",
  "theme": "Culture",
  "dateDebut": "07/27/2019",
  "dateFin": "07/30/2019",
  "prix": 1009,
  "promotion": true
}, {
  "ID": 9,
  "titre": "So far away from LA",
  "description": "Quam quidem partem accusationis admiratus sum et",
  "imageUrl": "https://picsum.photos/400?image=1050",
  "theme": "Farniente",
  "dateDebut": "03/18/2019",
  "dateFin": "05/25/2019",
  "prix": 3355,
  "promotion": false
}, {
  "ID": 10,
  "titre": "Paris en août",
  "description": "Thalassius vero ea tempestate praefectus praetorio",
  "imageUrl": "https://picsum.photos/400?image=1047",
  "theme": "Exotisme",
  "dateDebut": "08/05/2019",
  "dateFin": "08/12/2019",
  "prix": 324,
  "promotion": false
}, {
  "ID": 11,
  "titre": "Rome au printemps",
  "description": "Et quia Montius inter dilancinantium manus",
  "imageUrl": "https://picsum.photos/400?image=1013",
  "theme": "Culture",
  "dateDebut": "05/01/2019",
  "dateFin": "05/14/2019",
  "prix": 590,
  "promotion": true
}, {
  "ID": 12,
  "titre": "J'irai pas à Vesoul",
  "description": "Nunc vero inanes flatus quorundam vile esse",
  "imageUrl": "https://picsum.photos/400?image=1040",
  "theme": "Exotisme",
  "dateDebut": "06/01/2019",
  "dateFin": "06/24/2019",
  "prix": 22,
  "promotion": false
}, {
  "ID": 13,
  "titre": "Dans le port d'Amsterdam",
  "description": "Inter quos Paulus eminebat notarius ortus in",
  "imageUrl": "https://picsum.photos/400?image=1005",
  "theme": "Authentique",
  "dateDebut": "12/22/2019",
  "dateFin": "12/27/2019",
  "prix": 239,
  "promotion": false
}, {
  "ID": 14,
  "titre": "Aventures en Colombie",
  "description": "Dein Syria per speciosam interpatet diffusa",
  "imageUrl": "https://picsum.photos/400?image=1031",
  "theme": "Aventure",
  "dateDebut": "08/21/2019",
  "dateFin": "09/22/2019",
  "prix": 5902,
  "promotion": true
}, {
  "ID": 15,
  "titre": "Rodez en hiver",
  "description": "Tantum autem cuique tribuendum, primum quantum",
  "imageUrl": "https://picsum.photos/400?image=1036",
  "theme": "Aventure",
  "dateDebut": "01/22/2019",
  "dateFin": "02/29/2019",
  "prix": 890,
  "promotion": true
}, {
  "ID": 16,
  "titre": "Bruxelles en novembre",
  "description": "Et olim licet otiosae sint tribus pacataeque",
  "imageUrl": "https://picsum.photos/400?image=1033",
  "theme": "Farniente",
  "dateDebut": "11/26/2019",
  "dateFin": "11/30/2019",
  "prix": 234,
  "promotion": false
}, {
  "ID": 17,
  "titre": "Frissons au Népal",
  "description": "Huic Arabia est conserta, ex alio latere Nabataeis",
  "imageUrl": "https://picsum.photos/400?image=961",
  "theme": "Aventure",
  "dateDebut": "12/01/2019",
  "dateFin": "12/21/2019",
  "prix": 9232,
  "promotion": false
}, {
  "ID": 18,
  "titre": "Meurtres à Venise",
  "description": "Sed saepe enim redeo ad Scipionem, cuius omnis",
  "imageUrl": "https://picsum.photos/400?image=979",
  "theme": "Aventure",
  "dateDebut": "10/26/2019",
  "dateFin": "10/27/2019",
  "prix": 1643,
  "promotion": false
}, {
  "ID": 19,
  "titre": "Sur la route de Memphis",
  "description": "Eminuit autem inter humilia supergressa iam",
  "imageUrl": "https://picsum.photos/400?image=998",
  "theme": "Culture",
  "dateDebut": "10/15/2019",
  "dateFin": "10/24/2019",
  "prix": 623,
  "promotion": false
}, {
  "ID": 20,
  "titre": "Le Caire nid d'espions",
  "description": "Haec subinde Constantius audiens et quaedam",
  "imageUrl": "https://picsum.photos/400?image=947",
  "theme": "Exotisme",
  "dateDebut": "10/06/2019",
  "dateFin": "10/10/2019",
  "prix": 2344,
  "promotion": true
}];
},{}],"app.js":[function(require,module,exports) {
"use strict";

var _data = _interopRequireDefault(require("/data.json"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var elSejours = document.getElementById('sejours');
var triprix = document.getElementById('triprix');
elSejours.innerHTML = _data.default.map(function (element) {
  return "\n    <h1>".concat(element.titre, "</h1> \n    <img src =\"").concat(element.imageUrl, "\">\n    <h2>").concat(element.prix, " euros </h2>\n");
}).join('');
triprix.addEventListener('change', function () {
  if (triprix.checked) {
    var copiedata = _data.default.slice();

    elSejours.innerHTML = copiedata.sort(function (a, b) {
      return a.prix - b.prix;
    }).map(function (element) {
      return "\n       <h1>".concat(element.titre, "</h1> \n       <img src =\"").concat(element.imageUrl, "\">\n       <h2>").concat(element.prix, " euros </h2>\n   ");
    }).join('');
  } else {
    elSejours.innerHTML = _data.default.map(function (element) {
      return "\n    <h1>".concat(element.titre, "</h1> \n    <img src =\"").concat(element.imageUrl, "\">\n    <h2>").concat(element.prix, " euros </h2>\n    ");
    }).join('');
    console.log(_data.default);
  }
});
},{"/data.json":"data.json"}],"../../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "42679" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else {
        window.location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["../../../../usr/local/lib/node_modules/parcel-bundler/src/builtins/hmr-runtime.js","app.js"], null)
//# sourceMappingURL=/app.c328ef1a.js.map