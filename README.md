# Évaluation compétence 3 : Développer une interface utilisateur web dynamique

## Réalisation :
**Durée :** 5 avril 9h00 - 5 avril 17h00 (tout commit au-delà de cette limite ne sera pas pris en compte.)  
**Groupe :** En solo  
**Contraintes :**  Utiliser les données du fichier data.json fourni sur ce repository. Pas de framework JavaScript, pas de JQuery. (Sass et frameworks CSS autorisés.) 

## Rendu : 
Créer une branche à votre nom sur ce repository. Votre branche devra contenir le code source du site et un fichier README.md expliquant comment le lancer en local.



## Description du projet : 

**Mise en place d'une page de visualisation de séjours touristiques :**  
* La page devra contenir un header et un footer basiques (logo + titre du site + lien fictif vers mentions légales).
* Le contenu devra s'adapter à la taille de la fenêtre.
* Le contenu devra correspondre aux user stories suivantes :  
    * En tant qu'utilisateur, je peux voir la liste de tous les séjours 
    * En tant qu'utilisateur, je peux voir trois séjours en promotion dans une section en haut de la page (cette section devra rester visible en permanence.)
    * En tant qu'utilisateur, je peux trier les séjours par prix 
    * En tant qu'utilisateur, je peux trier les séjours par date de début 
    * En tant qu'utilisateur, je peux filtrer les séjours par thème pour ne voir que le thème qui m'intéresse
    * BONUS : En tant qu'utilisateur, je peux marquer des séjours comme favoris et les visualiser dans une section séparée sur une partie de la page.

**Chaque composant "séjour" devra contenir :**
* Le titre
* La description
* L'image
* Les dates de début et de fin
* Le prix
* Un bouton "favori".
  


## Critères d'évaluation du REAC (à titre indicatif) :
* Les pages web respectent la charte graphique de l'entreprise
* Les pages web sont conformes à l’expérience utilisateur y compris pour l’expérience mobile.
* L'architecture de l'application répond aux bonnes pratiques de développement et de sécurisation d'application web
* L’application web est optimisée pour les équipements mobiles
* Le code source est documenté ou auto-documenté