import data from "/data.json";

let elSejours = document.getElementById('sejours');
const triprix = document.getElementById('triprix');

elSejours.innerHTML = data.map(element => `
    <h1>${element.titre}</h1> 
    <img src ="${element.imageUrl}">
    <h2>${element.prix} euros </h2>
` 
).join('')

triprix.addEventListener('change', function(){
    if (triprix.checked){
       let copiedata = data.slice()

       elSejours.innerHTML = copiedata
       .sort(((a,b) => a.prix - b.prix))
       .map(element => `
       <h1>${element.titre}</h1> 
       <img src ="${element.imageUrl}">
       <h2>${element.prix} euros </h2>
   ` 
   ).join('')
    }else{
        elSejours.innerHTML = data.map(element => `
    <h1>${element.titre}</h1> 
    <img src ="${element.imageUrl}">
    <h2>${element.prix} euros </h2>
    ` 
        ).join('')
        console.log(data)
    }
})
